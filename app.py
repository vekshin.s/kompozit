from flask import Flask, render_template, request
from tensorflow import keras
import pickle
import numpy as np

app = Flask(__name__)

mod_MU = pickle.load(open('models/data_MU', 'rb'))
mod_PR = pickle.load(open('models/data_PR', 'rb'))
mod_MX = keras.models.load_model("models/Matrix_model")
sc_MU = pickle.load(open('models/sc_MU', 'rb'))
sc_PR = pickle.load(open('models/sc_PR', 'rb'))
sc_MX = pickle.load(open('models/sc_MX', 'rb'))

@app.route('/')
def index():
    return render_template('test1.html', name='User')


@app.route('/user/<name>/')
def user_profile(name):
    return f'Profile page of {name}'


@app.route('/MuPr/', methods=['post', 'get'])
def MuPr():
    message1 = 'Прогноз параметров "Модуль упругости при растяжении" и "Прочность при растяжении"'
    message = 'Введите данные для расчета '
    message2 = ''
    if request.method == 'POST':
        mtrx = request.form.get('mtrx')
        pl = request.form.get('pl')
        mupr = request.form.get('mupr')
        otv = request.form.get('otv')
        epgr = request.form.get('epgr')
        tvsp = request.form.get('tvsp')
        ppl = request.form.get('ppl')
        psm = request.form.get('psm')
        ugn = request.form.get('ugn')
        shn = request.form.get('shn')
        pln = request.form.get('pln')
        inp_lst = [mtrx,pl,mupr,otv,epgr,tvsp,ppl,psm,ugn,shn,pln]
        for i in inp_lst:
            if i == 1:
                message = 'Введите значения во все поля'
                break
        resul1 = sc_MU.inverse_transform(mod_MU.predict(np.array(inp_lst).reshape(1,-1)).reshape(-1,1))
        resul2 = sc_PR.inverse_transform(mod_PR.predict(np.array(inp_lst).reshape(1,-1)).reshape(-1,1))
        message = f'"Модуль упругости при растяжении" = {round(resul1[0][0],2)} ГПа'
        message2 = f'"Прочность при растяжении" = {round(resul2[0][0],2)} МПа'
    return render_template('MuPr.html', message=message, message1=message1, message2=message2)


@app.route('/Matrix/', methods=['post', 'get'])
def Mtrx():
    message1 = 'Прогноз параметра "Соотношение матрица-наполнитель"'
    message = 'Введите данные для расчета'
    if request.method == 'POST':
        pl = request.form.get('pl')
        mupr = request.form.get('mupr')
        otv = request.form.get('otv')
        epgr = request.form.get('epgr')
        tvsp = request.form.get('tvsp')
        ppl = request.form.get('ppl')
        mu = request.form.get('mu')
        pr = request.form.get('pr')
        psm = request.form.get('psm')
        ugn = request.form.get('ugn')
        shn = request.form.get('shn')
        pln = request.form.get('pln')
        inp_lst = [pl,mupr,otv,epgr,tvsp,ppl, mu, pr, psm,ugn,shn,pln]
        inp_lst = list(map(float, inp_lst))
        for i in inp_lst:
            if i == 1:
                message = 'Введите значения во все поля'
                break
        resul3 = mod_MX.predict(np.array(inp_lst))
        message = f'"Соотношение матрица-наполнитель" = {round(resul3[0][0],2)} '
        #message = inp_lst
    return render_template('Mtrx.html', message=message, message1=message1)


if __name__ == '__main__':
    app.run()
